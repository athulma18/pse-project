﻿//-----------------------------------------------------------------------
// <copyright file="Program.cs" company="B'15, IIT Palakkad">
//      Open Source. Feel free to use the code, but don't forget to acknowledge. 
// </copyright>
// <author>
//      Harsh
// </author>
// <review>
//      not reviewed
// </review>
//-----------------------------------------------------------------------

namespace Masti.Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    /// <summary>
    /// Contains entry point program for schema team
    /// </summary>
    public static class Program
    {
        /// <summary>
        /// Entry point for testing Schema code
        /// </summary> 
        public static void Main()
        {
            ImageSchemaTest imageTest = new ImageSchemaTest();

            // Console.WriteLine("Image Schema tests have begun!");
            imageTest.Run("C:/Users/Akshat/pse-project/Schema/ImageSchemaTestData/TestData.txt");
            MessageSchemaTest messageTest = new MessageSchemaTest();

            // Console.WriteLine("Message Schema tests have begun!");
            messageTest.Run("C:/Users/Akshat/pse-project/Schema/MessageSchemaTestData/TestData.txt");

           // Console.ReadKey();
        } 
    }
}
