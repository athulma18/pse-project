# Team Name

## Members

|               |                    |
| ------------- | ------------------ |
| Varshith Polu | Leader             |
| Jayaprakash   | Coder and Reviewer |
| M Aditya      | Coder              |
| Durga Prasad  | Coder              |
| Vinay Krishna | Coder              |

## Objectives

- Create user interfaces for server and client which enable messaging , image and video sharing

## Dependencies

My module depends on the following modules.

- **Messaging**: we subscribe to messaging to give us messages and send them.

- **Messaging**: we subscribe to image processing module to send and receive messages and screen sharing.

## Class Diagram

### Cient

![Class Diagram](111501016Aditya/ClientClassDiagram.png)

### Server

![Class Diagram](111501010Jayaprakash/ServerClassDiagram.png)

## Activity Diagram

![Activity Diagram](ReadMe/activity.png)

### Design Decisions

**Google API vs Normal API to login:**

​ Google API has been rejected because the application is intended to work in a LAN. without the access to internet

**Accommodating screen sharing , chat and messaging in one screen:**

​ The reason is using multiple windows complicate the project and might delay the project delivery.

### Design Patterns

We will be using publisher subscriber model where we would be subscribing to messaging and image processing modules.

## Internal Components

### Server Side :

### ![Class Diagram](111501010Jayaprakash/AfterLogin.png)

![Class Diagram](111501010Jayaprakash/BeforeLogin.png)

![Class Diagram](111501010Jayaprakash/ScreenShare.png)

### Client Side

## ![Class Diagram](111501016Aditya/after_Connect.png)

![Class Diagram](111501016Aditya/before_Connect.png)

## Work Distribution

### Varshith:

● Managing and Ensuring clean code

● Server Side Message history and previous chat section ui design and Coding the required functions

### Jayaprakash :

● Server Side messaging and text processing ui elements design

● Code the functions based on events using message processing API

### Aditya :

● Client Side messaging and text processing ui elements design

● Code the functions based on events using message processing API

### Durga Prasad :

● Login User interface elements design

● Implement Login using the provided API

### Vinay :

● Screen sharing UI element Design

● Code the functions based on events using Image processing API for screen sharing on both client and server side

---
