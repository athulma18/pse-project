﻿// -----------------------------------------------------------------------
//  <copyright file="Communication.cs" company="B'15, IIT Palakkad">
//      Open Source. Feel free to use the code, but don't forget to acknowledge. 
//  </copyright>
//  <Module>Netwoking Module</Module>
// <Author>Libin</Author>
// <Author>Parth</Author>
// -----------------------------------------------------------------------

namespace Masti.Networking
{
    using System;
    using System.Collections.Generic;
    using System.Globalization;
    using System.Net;
    using Masti.QualityAssurance;

    /// <summary>
    /// Communicator class implementing ICommunication Interface
    /// </summary>
    public partial class Communication : ICommunication
    {

        ITelemetryCollector networkTelemetryCollector;
        /// <summary>
        /// When the module is instantiated as student, it stores whether the socket to professor is working or not.
        /// When the module is instantiated as professor, it stores whether the professor is listening or not.
        /// </summary>
        public bool Connected => this.isRunning;

        /// <summary>
        /// Method to send data transfer request.
        /// </summary>
        /// <param name="msg">Data to to be send. Data must be encoded with <see cref="ISchema.Encode"/></param>
        /// <param name="targetIP">Recipient IP</param>
        /// <param name="type">Will be used to find component that will notified for message status.</param>
        /// <returns>success status</returns>
        public bool Send(string msg, IPAddress targetIP, DataType type)
        {
            if (targetIP == null)
            {
                MastiDiagnostics.LogError(string.Format(CultureInfo.CurrentCulture, "ArgumentNullException : Parmeter fromIP is Null"));
                return false;
            }
            NetworkTelemetry NetworkTelemetry = (NetworkTelemetry)this.networkTelemetryCollector.GetTelemetryObject("NetworkTelemetry");
            
            // Extract tag assciated with msg
            Tuple<bool, DataType> dataType = this.GetTagFromData(msg);

            // enque msg if passed DataType matches with extracted one
            if (dataType.Item1 == true && dataType.Item2 == type)
            {
                Packet request = new Packet(msg, targetIP, type);
                SendRequestQueue.Enqueue(request);
                NetworkTelemetry.DataForSendRecevied(msg, type);

                // wakes Up data outgoing thread if sleeping  
                this.queueEvent.Set();

                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// Stops all the sockets and main threads.
        /// </summary>
        public void StopCommunication()
        {
            this.Stop();
        }

        /// <summary>
        /// Extract tag from the data using Schema.
        /// </summary>
        /// <param name="data">data whose tag will be extracted</param>
        /// <returns>Tuple's first value tells whether valid tag is associated with data or not, second value is tag.</returns>
        private Tuple<bool, DataType> GetTagFromData(string data)
        {
            // If data is empty
            if (string.IsNullOrEmpty(data))
            {
                return Tuple.Create(false, DataType.Message);
            }
            else
            {
                // Decode msg using Schema
                IDictionary<string, string> decodeResult = this.schema.Decode(data, true);

                // Finding type of Data received
                if (decodeResult["type"].Equals("ImageProcessing", StringComparison.OrdinalIgnoreCase))
                {
                    return Tuple.Create(true, DataType.ImageSharing);
                }
                else if (decodeResult["type"].Equals("Messaging", StringComparison.OrdinalIgnoreCase))
                {
                    return Tuple.Create(true, DataType.Message);
                }
                else
                {
                    return Tuple.Create(false, DataType.Message);
                }
            }
        }
    }
}
