﻿// -----------------------------------------------------------------------
// <author> 
//      Jude K Anil
// </author>
//
// <date> 
//      03-11-2018 
// </date>
// 
// <reviewer>
//      Libin N George
//      Parth Patel
//      Ayush Mittal
// </reviewer>
//
// <copyright file="SendLargeTextNotifySuccess1Test.cs" company="B'15, IIT Palakkad">
//      This project is licensed under GNU General Public License v3. (https://fsf.org)
// </copyright>
//
// <summary> 
//      This file is a part of Networking Module Unit Testing.
// </summary>
// -----------------------------------------------------------------------

namespace Masti.Networking.UnitTests
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Net;
    using System.Text;
    using System.Threading;
    using System.Threading.Tasks;
    using QualityAssurance;
    using Schema;

    /// <summary>
    /// Test sends message with text but with invalid IP.
    /// The test fails if Failure status is not assigned as
    /// status in the status handler. The sending and the receiving
    /// communicator module used is the Student side communicator.
    /// </summary>
    internal class SendLargeTextNotifySuccess1Test : ITest
    {
        /// <summary>
        /// Event to signal the run function thread after calling the send function.
        /// </summary>
        private static ManualResetEvent mre = new ManualResetEvent(false);

        /// <summary>
        /// Invalid IP as target IP address.
        /// </summary>
        private IPAddress targetIP = IPAddress.Parse("127.0.0.1");

        /// <summary>
        /// The Professor communicator module to be tested.
        /// </summary>
        private ICommunication communicator = CommunicationFactory.GetCommunicator(serverIP: "127.0.0.1", serverPort: 1250);

        /// <summary>
        /// Message text to be sent.
        /// </summary>
        private string text = "a";

        /// <summary>
        /// Stores the result of the test. True for Success, false otherwise.
        /// </summary>
        private bool result = true;

        /// <summary>
        /// Stores the duration for which the thread should wait before triggering timeout.
        /// </summary>
        private TimeSpan interval = new TimeSpan(hours: 0, minutes: 0, seconds: 2);

        /// <summary>
        /// Stores the schema object.
        /// </summary>
        private ISchema schema;

        /// <summary>
        /// Stores the logger object.
        /// </summary>
        private ILogger logger;

        /// <summary>
        /// Initializes a new instance of the <see cref="SendLargeTextNotifySuccess1Test" /> class
        /// </summary>
        /// <param name="logger">Stores the logger object given.</param>
        public SendLargeTextNotifySuccess1Test(ILogger logger)
        {
            this.logger = logger;
            this.schema = new MessageSchema();
        }

        /// <summary>
        /// Executes the test and is part of ITest interface. It is called by the test harness.
        /// </summary>
        /// <returns>True if test is success, false otherwise.</returns>
        public bool Run()
        {
            try
            {
                this.communicator.SubscribeForDataStatus(type: DataType.Message, statusHandler: this.StatusHandler);
                this.logger.LogInfo("Subscribed for data status.");

                this.communicator.SubscribeForDataReceival(type: DataType.Message, receivalHandler: this.ReceivalHandler);
                this.logger.LogInfo("Subscribed for data Receival.");

                for (int i = 0; i < 1024; i++)
                {
                    Dictionary<string, string> dict = new Dictionary<string, string>();
                    dict.Add(this.text, "Messaging");

                    string message = this.schema.Encode(dict);
                    this.logger.LogInfo("Text encoded by the Schema object.");

                    this.communicator.Send(msg: message, targetIP: this.targetIP, type: DataType.Message);
                    this.logger.LogInfo($"Send function called with message of length: {message.Length.ToString()}");

                    // Reset the event to wait for receival of the send message.
                    mre.Reset();

                    if (!mre.WaitOne(this.interval))
                    {
                        throw new TimeoutException();
                    }

                    this.text += "a";
                }
            }
            catch (TimeoutException t)
            {
                this.logger.LogInfo($"Time out Exception caught. Exception raised is: {t.ToString()}");
                this.result = false;
            }
            catch (TextDoesNotMatchException r)
            {
                this.logger.LogInfo($"Text mismatch Exception causght. Exception raised is: {r.ToString()}");
                this.result = false;
            }
            catch (Exception e)
            {
                this.logger.LogError($"General exception raised: {e.ToString()}");
                this.result = false;
            }

            return this.result;
        }

        /// <summary>
        /// Delegate function to be called by the commmunication module. It is
        /// called to notify status of message waiting to be sent by the communicator.
        /// </summary>
        /// <param name="data">Stores the data of the message.</param>
        /// <param name="statusCode">Stores the status of the message to be sent.</param>
        private void StatusHandler(string data, StatusCode statusCode)
        {
            this.logger.LogInfo($"StatusHandler called with data: {data} and status: {statusCode.ToString()}");
            if (statusCode.Equals(StatusCode.Failure))
            {
                this.result = false;
            }

            return;
        }

        /// <summary>
        /// Delegate function to be called by the commmunication module. It is
        /// called when the communicator receives a new message.
        /// </summary>
        /// <param name="data">Stores the data of the message received.</param>
        /// <param name="fromIP">Stores the IP address from which the message was received.</param>
        private void ReceivalHandler(string data, IPAddress fromIP)
        {
            this.logger.LogInfo(message: $"ReceivalHandler called with data: {data} and IPAddress: {fromIP.ToString()}");

            IDictionary<string, string> dict = this.schema.Decode(data, false);
            if (!dict["Messaging"].Equals(this.text, StringComparison.Ordinal))
            {
                this.logger.LogInfo("Text received does not match text sent.");

                throw new TextDoesNotMatchException($"Sent text: {this.text}; Received text: {dict["Messaging"]}");
            }

            mre.Set();
            return;
        }
    }
}
